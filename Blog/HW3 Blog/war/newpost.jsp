<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.googlecode.objectify.*" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%@ page import="blog.Posting" %>
<%@ page import="blog.BlogUser" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Iterator" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EE 461L Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
  </head>
 
  <body>
  <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="/">Home</a>
          <a class="blog-nav-item" href="#">Create new Post</a>
          <a class="blog-nav-item" href="#">View all Posts</a>
        </nav>
      </div>
  </div>
  
   <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">The EE 461L Blog</h1>
        <p class="lead blog-description">a blog with Bootstrap.</p>
      </div>

      <div class="row">
	<% 
	UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
	boolean isUser = false;
	ObjectifyService.register(BlogUser.class);
	List<BlogUser> blogusers = ObjectifyService.ofy().load().type(BlogUser.class).list();
	for(BlogUser b : blogusers){
		if(b.getEmail().equals(user.getEmail())){
			isUser = true;
			break;
		}
	}
	if(user != null){
	%>
        <form action="/newpost" method="post">
      <div>
        <h3>Title</h3>
          <input name="title" type="text" required="required" id="title" size="60">
        <h3>&nbsp;</h3>
        <h3>Post        </h3>
        <p>
          <textarea name="content" cols="120" rows="20"></textarea>
        </p>
      </div>
      <div><input type="submit" value="Create New Post" /> 
      &nbsp;&nbsp;&nbsp;<a href="/">Cancel</a></div>
    </form>
    <%} %> 
       </div><!-- /.row -->

    </div><!-- /.container -->

    <div class="blog-footer">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>  
  </body>
</html>
