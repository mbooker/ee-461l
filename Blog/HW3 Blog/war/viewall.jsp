<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.googlecode.objectify.*" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%@ page import="blog.Posting" %>
<%@ page import="blog.BlogUser" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Iterator" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

 <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EE 461L Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
  </head>
 
  <body>
  <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="/">Home</a>
          <a class="blog-nav-item" href="#">Create new Post</a>
          <a class="blog-nav-item" href="#">View all Posts</a>
        </nav>
      </div>
  </div>
  
   <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">The EE 461L Blog</h1>
        <p class="lead blog-description">a blog with Bootstrap.</p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">
        
 
<%
    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
      pageContext.setAttribute("user", user);
%>
<%
    } else {
%>
<%
    }
%>
 <!-- /.blog-post -->

          
<%
	ObjectifyService.register(Posting.class);
	ObjectifyService.register(BlogUser.class);
	List<Posting> postings = ObjectifyService.ofy().load().type(Posting.class).list();   
	Collections.sort(postings); 
    if (postings.isEmpty()) {
 %>
        <h2 class="blog-post-title">No Blog Posts.</h2>
        <%
    } else {
        %>
        <%
        Iterator<Posting> postingit = postings.iterator();
        for(int i = 0; postingit.hasNext(); i += 1){
        	Posting p = postingit.next();
        	pageContext.setAttribute("posting_content", p.getContent());
        	pageContext.setAttribute("posting_title", p.getTitle());
        	pageContext.setAttribute("posting_user", p.getUser());
        	pageContext.setAttribute("posting_date", p.getDate());
         %>
         <div class="blog-post">
        	<h2 class="blog-post-title">${fn:escapeXml(posting_title)}</h2>
        	<p class="blog-post-meta">${fn:escapeXml(posting_date)} by <b>${fn:escapeXml(posting_user.nickname)}</b></p>
        	<p>${fn:escapeXml(posting_content)}</p>
        </div><!-- /.blog-post -->
        
              <%
        }
        }
%>
	<ul class="pager">
            <li><a href="/">Home</a></li>
          </ul>
  </div><!-- /.blog-main -->
  <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>This is a blog web application created for EE 461L. The authors are Michael Booker and Jeremy Clifton.</p>
          </div> 
          <div class="sidebar-module">
          <form method="post">
            <p>
              <%
          if (user == null) {
           %>
              <input name="subscribe" type="submit" id="subscribe value=" formaction="/subscribe" value="Subscribe"Subscribe">
              <%} else{ %>
              <input name="newpost" type="submit" id="newpost" formaction="/newpost.jsp" value="Create New Post">
            </p>
            <p>
              <input name="unsubscribe" type="submit" id="unsubscribe value=" formaction="/unsubscribe" value="Unsubscribe"Unsubscribe">
              <%} %>
            </p>
          </form>
          </div>
        </div><!-- /.blog-sidebar -->
      </div><!-- /.row -->

    </div><!-- /.container -->

    <div class="blog-footer">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>  
  </body>
</html>