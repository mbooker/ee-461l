<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EE 461L Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
  </head>
 
  <body>
  <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="/">Home</a>
          <a class="blog-nav-item" href="#">Create new Post</a>
          <a class="blog-nav-item" href="#">View all Posts</a>
        </nav>
      </div>
  </div>
  
   <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">The EE 461L Blog</h1>
        <p class="lead blog-description">a blog with Bootstrap.</p>
      </div>

      <div class="row">

        <div>
        <h1>Welcome to Blog! Thanks for subscribing! You can now create posts.</h1><hr/>
        <ul class="pager">
            <li><a href="/">Home</a></li>
          </ul>
        </div>
        
       </div><!-- /.row -->

    </div><!-- /.container -->

    <div class="blog-footer">
      <p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>  
  </body>
</html>