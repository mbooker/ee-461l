<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.googlecode.objectify.*" %>
<%@ page import="com.googlecode.objectify.ObjectifyService" %>
<%@ page import="blog.Posting" %>
<%@ page import="blog.BlogUser" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Iterator" %>
<%@ page import=" java.util.Properties" %>
<%@ page import=" javax.mail.Message" %>
<%@ page import=" javax.mail.MessagingException" %>
<%@ page import=" javax.mail.Session" %>
<%@ page import=" javax.mail.Transport" %>
<%@ page import=" javax.mail.internet.AddressException" %>
<%@ page import=" javax.mail.internet.InternetAddress" %>
<%@ page import=" javax.mail.internet.MimeMessage" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
Properties props = new Properties();
Session sessions = Session.getDefaultInstance(props, null);
Date d = new Date();
Calendar cal = Calendar.getInstance();
cal.setTime(d);
cal.add(Calendar.DATE, -1);
//Date d = cal.getTime();
System.out.printf(d.toString());
boolean newpostings = false;
String msgBody = "There are new Blog Posts for today: \n";
ObjectifyService.register(Posting.class);
List<Posting> postings = ObjectifyService.ofy().load().type(Posting.class).list();   
Collections.sort(postings); 
for( Posting p : postings){
	if(p.getDated().compareTo(d) > 0){
		newpostings = true;
		String temp = "\n" + p.getTitle() + "\n" + p.getDate() + " " + p.getUser().getNickname() + "\n" + p.getContent();
		msgBody.concat(temp);
	}
}

try {
    Message msg = new MimeMessage(sessions);
    msg.setFrom(new InternetAddress("michaelbooker.ut@gmail.com", "Michael Booker"));
    ObjectifyService.register(BlogUser.class);
	List<BlogUser> blogusers = ObjectifyService.ofy().load().type(BlogUser.class).list();
	for(BlogUser b : blogusers){
		msg.addRecipient(Message.RecipientType.TO,
                new InternetAddress(b.getEmail(), b.getName()));
		msg.setSubject("New Blog Posts for Today");
		msg.setText(msgBody);
		Transport.send(msg);	
	}
    

} catch (AddressException e) {
    // ...
} catch (MessagingException e) {
    // ...
}

%>