package blog;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class BlogUser {
	@Id String id;
	String email;
	String name;
	
	private BlogUser(){}
	public BlogUser(String name, String email){
		this.name = name;
		this.email = email;
		this.id = email;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public void setName(String name){
		this.name = name;
	}

	
	public String getEmail(){
		return(this.email);
	}
	public String getName(){
		return(this.name);
	}

}
