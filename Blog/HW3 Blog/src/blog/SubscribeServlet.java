package blog;

import com.googlecode.objectify.ObjectifyService;
import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
 
import java.io.IOException;
 
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SubscribeServlet extends HttpServlet{
	static {
        ObjectifyService.register(BlogUser.class);
    }
	public void doGet(HttpServletRequest req, HttpServletResponse res)throws IOException {       
		doPost(req, res);	
		}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    
    if (user != null) {
    	BlogUser bloguser = new BlogUser(user.getNickname(), user.getEmail());
    	ofy().save().entity(bloguser);
    	resp.sendRedirect("/subscribe.jsp");
    } else {
        resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
    }
}
}
