package blog;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.google.appengine.api.users.User;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
 
 
@Entity
public class Posting implements Comparable<Posting> {
    @Id Long id;
    User user;
    String content;
    String title;
    Date date;
    private static final String[] MONTH_ARRAY =
		{ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    private Posting() {}
    public Posting(User user, String content, String title) {
    	Calendar cal;
        this.user = user;
        this.content = content;
        this.title = title;
        date = new Date();
        cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("CST"));
        date = cal.getTime();
        System.out.println(new java.text.SimpleDateFormat("hh:mm:ss aa").format(date));
    }
    public User getUser() {
        return user;
    }
    public String getContent() {
        return content;
    }
    public String getTitle() {
        return title;
    }
    public String getDate(){
    	String s = new String(); 
    		s += MONTH_ARRAY[date.getMonth()] +". " + date.getDay() +", " + date.getYear();
    	//return (date.toString());
    	return (new java.text.SimpleDateFormat("MMM d, Y hh:mm aa").format(date));
    }
    public Date getDated(){
    	String s = new String(); 
    		s += MONTH_ARRAY[date.getMonth()] +". " + date.getDay() +", " + date.getYear();
    	//return (date.toString());
    	//return (new java.text.SimpleDateFormat("MMM d, Y hh:mm aa").format(date));
    	return date;
    }
    @Override
    public int compareTo(Posting other) {
        if (date.after(other.date)) {
            return -1;
        } else if (date.before(other.date)) {
            return 1;
        }
        return 0;
    }
}

