package blog;

import com.googlecode.objectify.ObjectifyService;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
 



import java.io.IOException;
import java.util.Collections;
import java.util.Date;
 

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BlogServlet extends HttpServlet{
	static {
        ObjectifyService.register(Posting.class);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
 

        String content = req.getParameter("content");
        
        //Posting posting = new Posting(user, content);
       // ofy().save().entity(posting);
        resp.sendRedirect("/blog.jsp");
    }

}
